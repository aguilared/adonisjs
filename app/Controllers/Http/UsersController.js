'use strict'
const User = use('App/Models/User')
const Book = use('App/Models/Book')

const { validate } = use('Validator')

class UsersController {

  async index({ response }) {
    //console.log('holasss')
    let users = await User.all()
    return response.json(users)
  }

  async index1({ response }) {
    const users = await User
      .query()
      .where('id', '>', 0)
      .fetch()
    return response.json(users)
  }

 /**          Store */
  async store({ request, response }) {
    const values = request.all()
    //console.log(values)
    const rules = {
      email: 'required|email|unique:users,email',
      password: 'required'
    }
    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      //return validation.messages()
      return response.status(400).json({ Error: validation.messages() })
    }

    const user = new User()
    user.username = values.username
    user.email = values.email
    user.password = values.password
    await user.save()

    return response.json(200, values);
  }

 /**          Update */
  async update({ request, response }) {
    const values = request.all()
    const rules = {
      email: 'required|email|unique:users,email',
      id: 'required',
      password: 'required'
    }
    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      //return validation.messages()
      return response.status(400).json({ Error: validation.messages() })
    }
    const user = await User.find(values.id)
    if (!user) {
      return response.status(404).json({ data: 'Resource not found' })
    }

    user.username = values.username
    user.email = values.email
    user.password = values.password
    await user.save()

    return response.status(200).json({ user })
  }

  async show({ request, response }) {
    const value = params.username

    const user = await User
      .query()
      .where('username', value)
      .fetch()

    var error = {};
    error.resp = 'No data from User';
    console.log(user)
    if (!user) {
      console.log('No data from Username : ' + value);
      return response.json(error)
    }
    //console.log(user);
    return response.status(200).json(user)
  }

  async showId({ params, response }) {
    const value = params.id
    const user = await User.find(params.id)

    var error = {};
    error.resp = 'No data From User Id='+value;
    console.log(user)
    if (!user) {
      console.log('No hay Usuario con ese Id: ' + value);
      return response.status(400).json(error)
    }
    return response.status(200).json(user)
  }

  async delete({ request, response }) {
    const values = request.all()
    const rules = {
      id: 'required'
    }
    const validation = await validate(request.all(), rules)
    console.log(values)
    if (validation.fails()) {
      //return validation.messages()
      return response.status(400).json({ Error: validation.messages() })
    }
    const user = await User.find(values.id)
    if (!user) {
      return response.status(404).json({ data: 'Resource not found' })
    }

    await user.delete()

    return response.status(204).json({ Response: 'Resource Deleted' })
  }

  async usersbooks({ request, response }) {
    const values = request.all()
    let users = await User.all()
    let books = await Book.all()

    console.log(users)
    console.log(users.length)

    return response.status(200).json({ Usuarios:users, Books:books })

  }

}

module.exports = UsersController


