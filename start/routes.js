'use strict'

const Route = use('Route')
const Book = use('App/Models/Book')

const User = use('App/Models/User')

Route.get('/', ({ request }) => {
  return { greeting: 'Hellworld in JSON' }
})

Route.get('userss', async () => {
  return await User.all()
})
Route.get('users', 'UsersController.index')
Route.get('usersbooks/:id', 'UsersController.usersbooks')
Route.post('users', 'UsersController.store')
Route.get('users/:username', 'UsersController.show')
Route.get('users_id/:id', 'UsersController.showId')
Route.put('users/:id', 'UsersController.update')
Route.delete('users/:id', 'UsersController.delete')


Route.post('books', 'BookController.store')
Route.get('books', 'BookController.index')
Route.get('books/:id', 'BookController.show')
Route.put('books/:id', 'BookController.update')
Route.delete('books/:id', 'BookController.delete')

Route.group(() => {


}).prefix('api/v1')
