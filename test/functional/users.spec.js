'use strict'
const { test, trait } = use('Test/Suite')('Users')
//const User = use('App/Models/User')

trait('Test/ApiClient')

test('get list of users', async ({ client }) => {

  const response = await client.get('/users').end()

  //response.assertStatus(200)
  response.assertJSONSubset([{
    title: 'Users App',
    body: 'list of users',
    //users: response
  }])
})
